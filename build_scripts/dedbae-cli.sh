# Exit on non-0 exit codes
set -e

rm -rf ./*.zip
rm -rf ./BUILTBY.txt
make clean

cp /opt/BUILTBY.txt ./

make
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"

zip -9 -j $ZIP_NAME LICENSE* README* BUILTBY.txt bin/*

cp $ZIP_NAME /var/www/bsnx/$JOB_NAME/
