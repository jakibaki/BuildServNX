# Exit on non-0 exit codes
set -e

rm -rf ./config.mk
cp config.mk.template config.mk

make clean
rm -rf ./*.zip
rm -rf ./BUILTBY.txt

cp /opt/BUILTBY.txt ./

make -j3
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"

zip -9 -r $ZIP_NAME LICENSE* README* BUILTBY.txt hactool

cp $ZIP_NAME /var/www/bsnx/$JOB_NAME/ 
