# Exit on non-0 exit codes
set -e

# This build has ** as branches, so it builds all branches that get updated
# Also it has post-build cleanup set up, deleting all zip files

make clean
rm -rf ./BUILTBY.txt

cp /opt/BUILTBY.txt ./

make -j3
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
#GIT_BRANCH_PROPER="$(echo $GIT_BRANCH  | cut -d "/" -f2)"
GIT_BRANCH_PROPER="master"
ZIP_NAME="$JOB_NAME-$GIT_BRANCH_PROPER-$SHORT_GIT.zip"

zip -9 $ZIP_NAME LICENSE* README* BUILTBY.txt sys-netcheat.*

cp $ZIP_NAME /var/www/bsnx/$JOB_NAME/
