# this script reinstalls switch packages, it's ran manually

# Disable exit on non-0 exit codes
set +e

# Find all switch packages
SWITCHLIBPACKAGES=$(pacman -Sl dkp-libs | grep 'switch' | awk '{print $2}' | tr '\n' ' ')
NXLIBPACKAGES=$(pacman -Sl dkp-libs | grep 'nx' | awk '{print $2}' | tr '\n' ' ')
DEVKITPACKAGES=$(pacman -Sl dkp-linux | grep 'devkit' | grep -v "keyring" | awk '{print $2}' | tr '\n' ' ')
SWITCHLINUXPACKAGES=$(pacman -Sl dkp-linux | grep 'switch' | awk '{print $2}' | tr '\n' ' ')

# Remove all switch packages
sudo pacman -Rs $SWITCHLINUXPACKAGES $SWITCHLIBPACKAGES $DEVKITPACKAGES $NXLIBPACKAGES general-tools --noconfirm

# Remove existing DKP stuff
sudo rm -rf /opt/devkitpro /opt/dkpstable

# Reinstall all switch packages
sudo pacman -Syu $SWITCHLINUXPACKAGES $SWITCHLIBPACKAGES $DEVKITPACKAGES $NXLIBPACKAGES general-tools --noconfirm

# Regenerate DKPStable
sudo cp -r /opt/devkitpro /opt/dkpstable

# Give ownership of DKP and DKPStable to jenkins
sudo chown -R jenkins:jenkins /opt/devkitpro
sudo chown -R jenkins:jenkins /opt/dkpstable

# libnx build will happen after this