# Exit on non-0 exit codes
set -e

# This workspace gets completely wiped on each build

# First things first, set the TitleID, the name to be used as result nsp and location of sample hbmenu loader.
TITLEID="0104204204201001"
NSP_FILENAME="nx-hbloader.nsp"
SAMPLE_HBMENU_LOCATION="/var/lib/jenkins/workspace/hacbrewpack/sample_hbmenu_loader"

# Get a short git hash
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)

# Add DKP tools to path (we'll need them for npdmtool and nacptool)
PATH=${DEVKITPRO}/tools/bin:$PATH

cp /opt/BUILTBY.txt .

# Patch nx-hbloader to make it run
sed -i -- 's/AppletType_LibraryApplet/AppletType_SystemApplication/g' source/main.c 

# Build nx-hbloader
make

# Go to build folder and copy hacbrewpack there
cd build
cp -f /var/lib/jenkins/workspace/hacbrewpack/hacbrewpack .

# Generate the required npdm, nacp, copy the icon and the amazing logos
mkdir control logo
npdmtool $SAMPLE_HBMENU_LOCATION/hbmenu.json exefs/main.npdm
nacptool --create "hbmenu" "Yellows8" "$SHORT_GIT-bsnx" "control/control.nacp" --titleid=$TITLEID
cp $SAMPLE_HBMENU_LOCATION/icon_AmericanEnglish.dat control/
cp /var/www/bsnx/.logo/* logo/

# Build the nsp
./hacbrewpack --noromfs -k /opt/prod.keys

# Copy the generated nsp with a custom and go back to base path
# If it failed to generate a proper nsp (got ("0"*16) etc), it'll fail to copy and therefore fail the build
cp hacbrewpack_nsp/$TITLEID.nsp ../$NSP_FILENAME
cd ..

# Generate a zip with NSP and other files
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"
zip -9 $ZIP_NAME README* LICENSE* BUILTBY.txt $NSP_FILENAME

# Copy the result to nx-hbloader folder
cp $ZIP_NAME /var/www/bsnx/nx-hbloader/
