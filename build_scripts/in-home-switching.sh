# Exit on non-0 exit codes
set -e

# ahhhhhhhhhhhhhhhh why
# requires RemoteFileSigLevel = Optional
sudo pacman -U https://cdn.discordapp.com/attachments/468194947401515010/525048848448487439/switch-ffmpeg-4.0.1-1-any.pkg.tar.xz --noconfirm

make clean
rm -rf ./*.zip
rm -rf ./BUILTBY.txt

cp /opt/BUILTBY.txt ./

DEVKITPRO="/opt/dkpstable" make
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"

zip -9 -r $ZIP_NAME LICENSE* README* BUILTBY.txt in-home-switching.*

cp $ZIP_NAME /var/www/bsnx/$JOB_NAME/
